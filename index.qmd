---
title: Dr. Chatbot -- the performance of openly accessible large language models in medicine

subtitle: Draft of the manuscript with webR
title-block-banner: true # "#145F7D" als Fakultäts-Farbe
# title-block-banner-color: "#F0F0F0" als weisse Schrift
theme:
  light: flatly
  dark: darkly
format: html
metadata-files: 
  - _authordata.yml # vor der finalen Online-Veröffentlichung noch hierhin kopieren
  - text/_plain-language-summary.md # die Gedankenstruktur des Manuskripts: siehe NOTIZ
filters:
  - authors-block # um den Autoren-Block auch im Word-Dokument zu haben
  - abstract-section # um den Abstract im normalen Text und nicht im YAML-Header zu schreiben
  - color-text.lua # Schriftfarben
  - webr # interaktiver R-Code
engine: knitr
webr: 
  show-startup-message: true
  packages: ['ggplot2', 'dplyr', 'readr']
keywords: 
  - Artificial Intelligence
  - Health Literacy
description: |
  Eine allgemeine Beschreibung des Projekts, das hinter dem Manuskript steht.
key-points:
  - Medizinische Ausbildung ist ein Querschnittsfach aus den Bereichen Medizin, Pädagogik und Psychologie.
  - Medical education is a multidisciplinary field of medicine, education, and psychology.
date: last-modified
citeproc: true
bibliography: text/references.bib
zotero: "chatgpt-health-literacy"
csl: bmc-medicine.csl # https://www.zotero.org/styles/bmc-medicine
citation-location: margin
number-sections: false
appendix-style: default
lightbox: auto
funding: 
  statement: "Der/die Autor*innen erhielt(en) für diese Arbeit keine spezielle Finanzierung."
lang: en
editor:
  markdown:
    canonical: true
---

## Abstract

{{< include text/_abstract.md >}}

------------------------------------------------------------------------

::: {.callout-caution title="IN PROGRESS ..."}
This manuscript is a work in progress. However, thank you for your interest. Please feel free to visit this web site again at a later date.

[*Dieses Manuskript ist noch in Arbeit. Wir danken Ihnen jedoch für Ihr Interesse. Bitte besuchen Sie diese Website zu einem späteren Zeitpunkt noch einmal ...*]{color="teal"}
:::

::: {.callout-tip title="STRUKTUR DES MANUSKRIPTS" collapse="true"}
[{{< meta plain-language-summary >}}]{color="gray"}
:::

{{< include text/Manuscript_LLM-comparison.txt >}}

## References {.unnumbered}

::: {#refs}
:::

## Declarations {.appendix}

{{< include text/_declarations.md >}}
