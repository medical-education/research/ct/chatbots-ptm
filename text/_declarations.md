
### Ethics approval and consent to participate

{{< lipsum 1 >}}

### Consent for publication

Not applicable

### Availability of data and materials

The original data that support the findings of this study are available from Open Science Framework (osf.io, see manuscript-URL).

### Competing interests

The authors declare that they have no competing interests.

### Funding

The author(s) received no specific funding for this work.

### Authors' contributions

{{< lipsum 1 >}}

### CRediT authorship contribution statement

**Hendrik Friederichs:** Conceptualization, Data curation, Formal analysis, Investigation, Methodology, Visualization, Supervision, Writing - review & editing, Writing - original draft.

### Acknowledgments

The manuscript was linguistically improved using DeepL Pro (DeepL SE, Cologne, Germany). DeepL Pro provided translation and language refinement only to ensure clarity and coherence in English.
 
[Das Manuskript wurde mit DeepL Pro (DeepL SE, Köln, Deutschland) sprachlich verbessert. DeepL Pro diente lediglich der Übersetzung und sprachlichen Verfeinerung, um die Klarheit und Kohärenz im Englischen zu gewährleisten.]{color="gray"}

The authors are grateful for the insightful comments offered by the anonymous peer reviewers at {{< meta citation.container-title >}}. The generosity and expertise of one and all have improved this study in innumerable ways and saved us from many errors; those that inevitably remain are entirely our own responsibility.