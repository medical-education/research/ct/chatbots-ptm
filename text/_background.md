---
bibliography: references.bib
---

### Broad problem

{{< lipsum 1 >}}

### Theoretical and/or empirical focus of the problem

#### Large Language Models

{{< lipsum 1 >}}

[{{< lipsum 1 >}}]{color="lightgray"}



 

### Focused problem statement

 {{< lipsum 1 >}}

### Statement of study intent

 {{< lipsum 1 >}}

We performed a study of medical students to investigate the following questions:

1.  What is ...
2.  Why are ...
