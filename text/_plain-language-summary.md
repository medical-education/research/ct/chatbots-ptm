
plain-language-summary: |

  **Relevantes Problem:** Gesundheitskompetenz beruht auf verschiedenen Fähigkeiten zur Informationsverarbeitung und ist als Grad, in dem Personen in der Lage sind, grundlegende Gesundheitsinformationen und -dienste zu erhalten, zu verarbeiten und zu verstehen, um angemessene Gesundheitsentscheidungen zu treffen, definiert. Sie ist für Patienten essentiell, um medizinische Berichte und Behandlungen zu verstehen. Für Ärzte kommt noch die Fähigkeiten Studienergebnisse zu verstehen, Patienten aufzuklären, klinische Entscheidungen zu treffen und effektiv im medizinischen Team zu kommunizieren, dazu.
  Künstliche Intelligenz nimmt auch in der Medizin einen immer breiteren Raum ein und kann sowohl Patienten als auch Ärzte in ihrem gesundheitsbezogenen Handeln unterstützen.
  
  **Fokussiertes Problem:** Gesundheitskompetenz, besteht aus mehreren Formen der Literacy, z. B. risk literacy, graph literacy und scientific literacy skills. Es ist bisher nicht bekannt, inwieweit Large Language Models diese Kompetenzen bezüglich medizinischer Fragestellungen beherrschen.
  
  **Gap des Problems:** 
  <!-- Gap / Dilemma / Widerspruch subj. Erwartung und Realität -->
  Health Literacy beeinflusst das Risikoverständnis und Entscheidungsverhalten, wobei Studien hauptsächlich Patienten betrachten. Gelegentlich werden auch Fachleute aus dem Gesundheitswesen betrachtet.
  
  **Lösung?:** 
  <!-- möglicher Fortschritt / mögliche Lösung - Ansprechen von Motiv(en): Anschluss, Leistung, Macht -->
  Für den Einsatz von KI bei Menschen mit geringerer Gesundheitskompetenz ist es wichtig, Kenntnisse über die Gesundheitskompetenz der eingesetzten Tools zu haben, um die Relevanz und Glaubwürdigkeit in entsprechenden Anwendungsfeldern einschätzen und für die daraus folgenden Entscheidungen berücksichtigen zu können.

  **Forschungsfragen:** 
  Daher wurde eine Studie mit ChatGPT durchgeführt, um deren Fähigkeit zu untersuchen.   
  
  **Studienpopulation:** ChatGPT
  
  **Studiendesign:** Validierte Fragebögen  
  
  **Datenerhebung:**  mittels BNT, Graph Literacy Scale und Test of Scientific Literacy Skills (TOSLS)
  
  **Ergebnisparameter:** Anzahl der richtigen Antworten insgesamt.
  
  **Statistik:**  Bestimmung der Prozentwerte für die absolute und relative Bewertung der Leistungen im Vergleich mit Patienten- und Fachkollektiven. Evtl. noch weitere Vergleichsberechnungen.




