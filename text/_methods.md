---
bibliography: text/references.bib
zotero: "graph-literacy-progress"
---

### Setting and subjects

{{< lipsum 1 >}}

### Study design

{{< lipsum 1 >}}

### Ethical approval

{{< lipsum 1 >}}

### Data collection

Data collection for this study was determined à priori as follows:

...

```{webr-r}
#| context: setup


```

### Outcome Measures

...

### Statistical methods

...

```{webr-r}
#| context: interactive
#| autorun: true

# Download a dataset
download.file(
  'https://gitlab.ub.uni-bielefeld.de/medical-education/research/ct/chatbots-ptm/-/raw/master/rawdata/reprex.csv',
  'rawdata.csv'
)
```

```{webr-r}
#| context: interactive
# Read the data
rawdata = read.csv("rawdata.csv", sep = ";")
```

```{webr-r}
data = rawdata |>  
  dplyr::select(ID, group, score, subscore1, subscore2) |>
  dplyr::mutate(
    score = score/100,
    subscore1 = subscore1/100,
    subscore2 = subscore2/100
  )
```

```{webr-r}
#| context: interactive
# Visualize data

data |>
  ggplot2::ggplot() +
  ggplot2::aes(x = factor(group), y = score) +
  ggplot2::geom_jitter(width = 0.05, alpha = 0.75, ggplot2::aes(color = factor(group))) +
  ggplot2::geom_boxplot(size = 0.5, alpha = 0, width = 0.2) +
  ggplot2::geom_violin(alpha = 0, width = 0.5) +
  ggplot2::stat_summary(fun = "mean", color = "#9d0208", size = 1) +
  ggplot2::stat_summary(ggplot2::aes(label=round(ggplot2::after_stat(y), 2)), fun = mean, geom="text", size=1.5, color = "white") +
  ggplot2::geom_hline(yintercept=0.6, linetype='dashed', col = '#9d0208') +
  ggplot2::labs(title = "Titel", #Labels for axis, legend and title
                x = "Beschreibung x-Achse", 
                y = "Beschreibung y-Achse",
                color = "Gruppe"
  ) +
  ggplot2::scale_y_continuous(limits = c(0, 1), 
                              breaks = c(0, 0.2, 0.4, 0.6, 0.8, 1), #y-scale labels
                              minor_breaks = c(0.1, 0.3, 0.5, 0.7, 0.9), 
                              labels = scales::percent
  ) + # nachfolgend Veränderungen der Grafik-Formatierungen
  ggplot2::theme_bw(base_size = 10) +
  ggplot2::theme(
    axis.title         = ggplot2::element_text(face = "bold"),
    axis.title.y.right = ggplot2::element_text(size = 8),
    legend.title       = ggplot2::element_text(face = "bold"),
    plot.title         = ggplot2::element_text(size = 12, face = "bold"),
    panel.border       = ggplot2::element_blank(),
    strip.text         = ggplot2::element_text(face = "bold")
  ) +
  ggplot2::scale_fill_brewer(palette = "Dark2") + 
  ggplot2::scale_color_brewer(palette = "Dark2")
```
