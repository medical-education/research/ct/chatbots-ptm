---
bibliography: text/references.bib
---

### Summary

After the evaluation of all datasets, the following findings emerged. The first is that ...

### Limitation: study population

{{< lipsum 1 >}}

### Limitation: study design

...

### Integration with prior work

Only a few studies provide insights 

### Implications for practice

{{< lipsum 1 >}}

### Implications for research

...

### Conclusions

...
