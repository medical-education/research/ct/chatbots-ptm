## Chatbots in progress testing

The integration of Large Language Models (LLMs) in medical education, particularly their use in medical examinations, represents a significant advancement in the intersection of artificial intelligence and medical training. Recent studies have evaluated the capabilities of LLMs, such as ChatGPT, in the context of medical licensing exams, including the United States Medical Licensing Examination (USMLE). These models have shown the ability to perform at or near the passing thresholds for various stages of the USMLE without specific training tailored to the exam. This highlights their potential as innovative tools for both medical education and clinical decision-making support (Kung et al., 2023). For example, Gilson et al. (2023) observed that ChatGPT achieved accuracies ranging from 42% to 64.4% across different sections of the USMLE. This indicates its ability to provide logically justified answers, which is a fundamental component of medical reasoning and decision-making. Additionally, the increasing importance and potential usefulness of large language models in improving medical education and assessment methodologies is highlighted by their superior performance compared to human scores in neurology board-style examinations (Performance of Large Language Models on a Neurology Board–Style Examination, JAMA Network). These findings indicate a positive outlook for LLMs in medical education. They can be used as supplementary tools to traditional learning and assessment methods, creating a more interactive and dynamic educational environment.

We want to have the publicly available chatbots solve a current progress test to test the performance of these tools for doctors, students and patients.



**References:**
- Gilson, A., Safranek, C. W., Huang, T., Socrates, V., Chi, L., Taylor, R. A., & Chartash, D. (2023). *JMIR Medical Education.*
- Kung, T. H., Cheatham, M., Medenilla, A., Sillos, C., De Leon, L., Elepaño, C., Madriaga, M., Aggabao, R., Diaz-Candido, G., Maningo, J., & Tseng, V. (2023). *PLOS Digital Health, 2*(2), e0000198.


03-2024