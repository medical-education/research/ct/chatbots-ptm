// Some definitions presupposed by pandoc's typst output.
#let blockquote(body) = [
  #set text( size: 0.92em )
  #block(inset: (left: 1.5em, top: 0.2em, bottom: 0.2em))[#body]
]

#let horizontalrule = [
  #line(start: (25%,0%), end: (75%,0%))
]

#let endnote(num, contents) = [
  #stack(dir: ltr, spacing: 3pt, super[#num], contents)
]

#show terms: it => {
  it.children
    .map(child => [
      #strong[#child.term]
      #block(inset: (left: 1.5em, top: -0.4em))[#child.description]
      ])
    .join()
}

// Some quarto-specific definitions.

#show raw.where(block: true): block.with(
    fill: luma(230), 
    width: 100%, 
    inset: 8pt, 
    radius: 2pt
  )

#let block_with_new_content(old_block, new_content) = {
  let d = (:)
  let fields = old_block.fields()
  fields.remove("body")
  if fields.at("below", default: none) != none {
    // TODO: this is a hack because below is a "synthesized element"
    // according to the experts in the typst discord...
    fields.below = fields.below.amount
  }
  return block.with(..fields)(new_content)
}

#let empty(v) = {
  if type(v) == "string" {
    // two dollar signs here because we're technically inside
    // a Pandoc template :grimace:
    v.matches(regex("^\\s*$")).at(0, default: none) != none
  } else if type(v) == "content" {
    if v.at("text", default: none) != none {
      return empty(v.text)
    }
    for child in v.at("children", default: ()) {
      if not empty(child) {
        return false
      }
    }
    return true
  }

}

#show figure: it => {
  if type(it.kind) != "string" {
    return it
  }
  let kind_match = it.kind.matches(regex("^quarto-callout-(.*)")).at(0, default: none)
  if kind_match == none {
    return it
  }
  let kind = kind_match.captures.at(0, default: "other")
  kind = upper(kind.first()) + kind.slice(1)
  // now we pull apart the callout and reassemble it with the crossref name and counter

  // when we cleanup pandoc's emitted code to avoid spaces this will have to change
  let old_callout = it.body.children.at(1).body.children.at(1)
  let old_title_block = old_callout.body.children.at(0)
  let old_title = old_title_block.body.body.children.at(2)

  // TODO use custom separator if available
  let new_title = if empty(old_title) {
    [#kind #it.counter.display()]
  } else {
    [#kind #it.counter.display(): #old_title]
  }

  let new_title_block = block_with_new_content(
    old_title_block, 
    block_with_new_content(
      old_title_block.body, 
      old_title_block.body.body.children.at(0) +
      old_title_block.body.body.children.at(1) +
      new_title))

  block_with_new_content(old_callout,
    new_title_block +
    old_callout.body.children.at(1))
}

#show ref: it => locate(loc => {
  let target = query(it.target, loc).first()
  if it.at("supplement", default: none) == none {
    it
    return
  }

  let sup = it.supplement.text.matches(regex("^45127368-afa1-446a-820f-fc64c546b2c5%(.*)")).at(0, default: none)
  if sup != none {
    let parent_id = sup.captures.first()
    let parent_figure = query(label(parent_id), loc).first()
    let parent_location = parent_figure.location()

    let counters = numbering(
      parent_figure.at("numbering"), 
      ..parent_figure.at("counter").at(parent_location))
      
    let subcounter = numbering(
      target.at("numbering"),
      ..target.at("counter").at(target.location()))
    
    // NOTE there's a nonbreaking space in the block below
    link(target.location(), [#parent_figure.at("supplement") #counters#subcounter])
  } else {
    it
  }
})

// 2023-10-09: #fa-icon("fa-info") is not working, so we'll eval "#fa-info()" instead
#let callout(body: [], title: "Callout", background_color: rgb("#dddddd"), icon: none, icon_color: black) = {
  block(
    breakable: false, 
    fill: background_color, 
    stroke: (paint: icon_color, thickness: 0.5pt, cap: "round"), 
    width: 100%, 
    radius: 2pt,
    block(
      inset: 1pt,
      width: 100%, 
      below: 0pt, 
      block(
        fill: background_color, 
        width: 100%, 
        inset: 8pt)[#text(icon_color, weight: 900)[#icon] #title]) +
      block(
        inset: 1pt, 
        width: 100%, 
        block(fill: white, width: 100%, inset: 8pt, body)))
}



#let article(
  title: none,
  authors: none,
  date: none,
  abstract: none,
  cols: 1,
  margin: (x: 1.25in, y: 1.25in),
  paper: "us-letter",
  lang: "en",
  region: "US",
  font: (),
  fontsize: 11pt,
  sectionnumbering: none,
  toc: false,
  toc_title: none,
  toc_depth: none,
  doc,
) = {
  set page(
    paper: paper,
    margin: margin,
    numbering: "1",
  )
  set par(justify: true)
  set text(lang: lang,
           region: region,
           font: font,
           size: fontsize)
  set heading(numbering: sectionnumbering)

  if title != none {
    align(center)[#block(inset: 2em)[
      #text(weight: "bold", size: 1.5em)[#title]
    ]]
  }

  if authors != none {
    let count = authors.len()
    let ncols = calc.min(count, 3)
    grid(
      columns: (1fr,) * ncols,
      row-gutter: 1.5em,
      ..authors.map(author =>
          align(center)[
            #author.name \
            #author.affiliation \
            #author.email
          ]
      )
    )
  }

  if date != none {
    align(center)[#block(inset: 1em)[
      #date
    ]]
  }

  if abstract != none {
    block(inset: 2em)[
    #text(weight: "semibold")[Abstract] #h(1em) #abstract
    ]
  }

  if toc {
    let title = if toc_title == none {
      auto
    } else {
      toc_title
    }
    block(above: 0em, below: 2em)[
    #outline(
      title: toc_title,
      depth: toc_depth
    );
    ]
  }

  if cols == 1 {
    doc
  } else {
    columns(cols, doc)
  }
}
#show: doc => article(
  title: [Assessment of health literacy of ChatGPT – a study to assess medical data interpretation skills of an AI],
  authors: (
    ( name: [Janina Soler Wenglein],
      affiliation: [Universität Bielefeld, Medizinische Fakultät OWL],
      email: [janina.soler\@uni-bielefeld.de] ),
    ( name: [Hendrik Friederichs],
      affiliation: [Universität Bielefeld, Medizinische Fakultät OWL],
      email: [hendrik.friederichs\@uni-bielefeld.de] ),
    ),
  date: [2024-01-31],
  lang: "en",
  abstract: [#strong[Background / Hintergrund];: Nunc ac dignissim magna. Vestibulum vitae egestas elit. Proin feugiat leo quis ante condimentum, eu ornare mauris feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris cursus laoreet ex, dignissim bibendum est posuere iaculis. Suspendisse et maximus elit. In fringilla gravida ornare. Aenean id lectus pulvinar, sagittis felis nec, rutrum risus. Nam vel neque eu arcu blandit fringilla et in quam. Aliquam luctus est sit amet vestibulum eleifend. Phasellus elementum sagittis molestie. Proin tempor lorem arcu, at condimentum purus volutpat eu. Fusce et pellentesque ligula. Pellentesque id tellus at erat luctus fringilla. Suspendisse potenti.

#strong[Methods / Methoden];: …

#strong[Results / Ergebnisse];: …

#strong[Conclusio / Schlussfolgerungen];: …

],
  toc_title: [Table of contents],
  toc_depth: 3,
  cols: 1,
  doc,
)
#import "@preview/fontawesome:0.1.0": *


#super[1] Universität Bielefeld, Medizinische Fakultät OWL

#super[✉] Correspondence: #link("mailto:hendrik.friederichs@uni-bielefeld.de")[Hendrik Friederichs \<hendrik.friederichs\@uni-bielefeld.de\>]

#block[
#callout(
body: 
[
This manuscript is a work in progress. However, thank you for your interest. Please feel free to visit this web site again at a later date.

#emph[Dieses Manuskript ist noch in Arbeit. Wir danken Ihnen jedoch für Ihr Interesse. Bitte besuchen Sie diese Website zu einem späteren Zeitpunkt noch einmal …]

]
, 
title: 
[
IN PROGRESS …
]
, 
background_color: 
rgb("#ffe5d0")
, 
icon_color: 
rgb("#FC5300")
, 
icon: 
fa-fire()
, 
)
]
#block[
#callout(
body: 
[
#strong[Relevantes Problem:] Gesundheitskompetenz beruht auf verschiedenen Fähigkeiten zur Informationsverarbeitung und ist als Grad, in dem Personen in der Lage sind, grundlegende Gesundheitsinformationen und -dienste zu erhalten, zu verarbeiten und zu verstehen, um angemessene Gesundheitsentscheidungen zu treffen, definiert. Sie ist für Patienten essentiell, um medizinische Berichte und Behandlungen zu verstehen. Für Ärzte kommt noch die Fähigkeiten Studienergebnisse zu verstehen, Patienten aufzuklären, klinische Entscheidungen zu treffen und effektiv im medizinischen Team zu kommunizieren, dazu. Künstliche Intelligenz nimmt auch in der Medizin einen immer breiteren Raum ein und kann sowohl Patienten als auch Ärzte in ihrem gesundheitsbezogenen Handeln unterstützen. \
#strong[Fokussiertes Problem:] Gesundheitskompetenz, besteht aus mehreren Formen der Literacy, z. B. risk literacy, graph literacy und scientific literacy skills. Es ist bisher nicht bekannt, inwieweit Large Language Models diese Kompetenzen bezüglich medizinischer Fragestellungen beherrschen. \
#strong[Gap des Problems:] Healt hLiteracy beeinflusst das Risikoverständnis und Entscheidungsverhalten, wobei Studien hauptsächlich Patienten betrachten. \
#strong[Lösung?:] Für den Einsatz von KI bei Menschen mit geringerer Gesundheitskompetenz ist es wichtig, Kenntnisse über die Gesundheitskompetenz der eingesetzten Tools zu haben, um die Relevanz und Glaubwürdigkeit in entsprechenden Anwendungsfeldern einschätzen und für die daraus folgenden Entscheidungen berücksichtigen zu können. \
#strong[Forschungsfragen:] Daher wurde eine Studie mit ChatGPT durchgeführt, um deren Fähigkeit zu untersuchen. \
#strong[Studienpopulation:] ChatGPT \
#strong[Studiendesign:] Validierte Fragebögen \
#strong[Datenerhebung:] mittels BNT, Graph Literacy Scale und Test of Scientific Literacy Skills \(TOSLS) \
#strong[Ergebnisparameter:] Anzahl der richtigen Antworten insgesamt. \
#strong[Statistik:] Bestimmung der Prozentwerte für die absolute und relative Bewertung der Leistungen im Vergleich mit Patienten- und Fachkollektiven. Evtl. noch weitere Vergleichsberechnungen.

]
, 
title: 
[
STRUKTUR DES MANUSKRIPTS
]
, 
background_color: 
rgb("#ccf1e3")
, 
icon_color: 
rgb("#00A047")
, 
icon: 
fa-lightbulb()
, 
)
]
== Background
<background>
=== Broad problem
<broad-problem>
Health Literacy

\[1\] This review evaluates the effectiveness of interventions aimed at improving health literacy \(HL) among migrants. It included 34 randomized controlled trials \(RCTs) involving 8249 participants, primarily in high-income countries. The interventions were culturally and linguistically adapted. Key findings include:

- Self-management programs \(SMPs) slightly improve self-efficacy and HIV-related HL, with minimal impact on knowledge or subjective health status.
- HL skills building courses \(HLSBC) may improve general HL and knowledge but have little effect on depression literacy or health behavior.
- Audio/visual education without personal feedback \(AVE) likely improves depression literacy and health service use, with uncertain effects on self-efficacy and knowledge.
- Adapted medical instruction may enhance understanding of health information with uncertain effects on medication adherence.
- SMPs compared to written information slightly improve health numeracy, print literacy, and self-efficacy. The review underscores the need for more robust studies, especially regarding long-term effects and gender differences in HL interventions for migrants.

\[2\] This systematic review and meta-analysis examined the effect of digital interventions on health literacy in people with chronic diseases. From 9386 articles, 17 studies representing 16 unique trials with 5138 participants were included. The studies targeted conditions like cancer, diabetes, cardiovascular disease, and HIV. The interventions varied, including skills training, websites, electronic records, remote monitoring, and education. The meta-analysis showed that digital interventions were better than usual care for eHealth literacy. However, the evidence is limited due to the heterogeneity in study designs, populations, and outcomes. The study highlights the need for more research on the effects of digital interventions on health literacy in chronic disease patients.

\[3\] This meta-analysis assessed the influence of e-health literacy and health-promoting behaviors on the spread of COVID-19. It involved 12 studies with 9854 participants. The study found strong evidence that e-health literacy and COVID-19 protective behaviors significantly reduce the spread of COVID-19 infection. The pooled standardized mean differences \(SMDs) for e-health literacy and COVID-19 protective behaviors were significant, indicating a strong positive impact. The study recommends prioritizing interventions and policies to promote e-literacy programs and preventative measures for effective COVID-19 management.

\[4\] This study conducted a meta-analysis to explore how health literacy influences health behaviors and outcomes, with a focus on the mediating role of belief-based constructs from social cognition theories. Using data from 203 studies involving over 210,000 participants, the research found that health literacy, attitudes, and self-efficacy \(a belief in one’s ability to succeed) are interconnected and together affect health behavior and outcomes. The findings suggest that health literacy influences health behavior and outcomes partly through these belief-based constructs. However, since the study is based on correlational data, further research using longitudinal or experimental designs is necessary to confirm these findings.

\[5\] This systematic review and meta-analysis assessed the impact of health literacy interventions on patients with diabetes. It involved 21 studies, focusing on changes in glycosylated hemoglobin \(HbA1c), medication adherence, systolic blood pressure, self-efficacy, and health literacy levels. The review found significant improvements in HbA1c levels and medication adherence among those who received health literacy interventions compared to usual care, but no notable change in systolic blood pressure. Additionally, self-efficacy and health literacy levels improved. However, the evidence quality for medication adherence was very low, and the reliability of these conclusions needs further confirmation.

\[6\] This rapid review and meta-analysis aimed to identify modifiable predictors of health literacy among working-age adults, especially those at risk of long-term unemployment. After reviewing 54 studies, the analysis identified several factors associated with higher health literacy: better language proficiency, more frequent internet use, using the internet for health information, increased physical activity, engaging in more oral health behaviors, watching health-related TV, and having good health status. The findings suggest that improving language proficiency and providing health information in multiple languages, along with accessible health information online and in traditional media, could be effective strategies to enhance health literacy in working-age populations.

\[7\] This study systematically reviewed and meta-analyzed gender differences in health literacy among individuals with a migration background. After examining 24 studies, the analysis revealed that women generally had higher health literacy scores than men. However, the results should be cautiously interpreted due to high study heterogeneity and less significant differences when excluding studies with high bias risk. The study highlights a lack of research on men and the social aspects of gender in health literacy among migrants.

\[8\] This systematic review and meta-analysis investigated the impact of health literacy on mortality, readmission, and quality of life in cardiovascular disease \(CVD) patients. Out of 1616 studies, 16 observational studies were included. The findings showed that low health literacy significantly increased mortality and hospital readmission risks and decreased quality of life in CVD patients. The study underscores the importance of addressing health literacy in clinical practice to improve CVD prognosis.

\[9\] This updated review examined how patient decision aids \(PtDAs) cater to individuals with low health literacy or those from socially-disadvantaged groups. Out of 213 randomized controlled trials, only 12% addressed these needs. Most PtDAs did not meet the recommended thresholds for understandability and actionability, and none were written at or below a sixth-grade reading level. The review concludes that PtDAs need more focus on health literacy and socially-disadvantaged populations to ensure equitable decision support.

\[10\] This systematic review and meta-analysis explored the relationship between health literacy and physical activity in older people. It found that older individuals with inadequate health literacy were 38% less likely to engage in physical activity for five or more days per week. However, there was no significant difference in the number of steps taken per day between those with adequate and inadequate health literacy, as per the two studies that used activity monitors. The study suggests enhancing health literacy could potentially improve physical activity among older adults.

\[11\] This systematic review and meta-analysis examined the prevalence of low health literacy in European Union countries. The study found that the prevalence of low health literacy varied from 27% to 48%, depending on the assessment method. Southern, Western, and Eastern EU countries showed lower health literacy compared to Northern Europe. Different assessment methods influenced the prevalence rates, with refugees having the lowest health literacy. The study concludes that low health literacy is a significant public health challenge in Europe, affecting one-third to almost half of the population.

\[12\] This systematic review and meta-analysis investigated the impact of health literacy on mortality, hospitalizations, and emergency department visits among heart failure patients. The study found that 24% of heart failure patients had inadequate or marginal health literacy, which was associated with a higher risk of mortality and hospitalizations. Even after adjusting for other factors, inadequate health literacy remained significantly associated with increased risk of death and hospitalizations. The study highlights the high prevalence of inadequate health literacy in heart failure patients and its association with worse outcomes, underscoring the need for health literacy-focused interventions.

\[13\] This study systematically reviewed the prevalence of limited health literacy among patients with type 2 diabetes mellitus across different countries. It found a wide variation in prevalence, ranging from 7.3% to 82%, with factors like the country of the study, the health literacy tool used, and the country’s region influencing these differences. In the USA, nearly one-third of patients with type 2 diabetes showed limited functional health literacy. The study highlighted the importance of healthcare interactions and educational level in managing diabetes.

\[14\] This research focused on the association between health literacy and mortality in patients with heart failure, particularly after hospitalization. The systematic review and meta-analysis of three cohort studies, involving 2,858 participants, revealed that limited health literacy was significantly associated with higher all-cause mortality. However, intervention studies did not show a clear link between limited health literacy and mortality. The study suggests future research should explore telehealth-based medicine for heart failure patients, especially those with limited health literacy.

\[15\] This study conducted a systematic review and meta-analysis to assess the relationship between health literacy and quality of life. It included 23 studies with 12,303 subjects and found a moderate correlation between health literacy and quality of life. The study observed that different dimensions of health literacy, such as health knowledge, behavior, belief, and skill, had varying degrees of correlation with quality of life. The findings were particularly pronounced among community residents, in China, and in cohort studies. The study concludes that more evidence is needed to fully understand the relationship between health literacy and quality of life.

\[16\] This study conducted a systematic review and meta-analysis to determine the prevalence of limited health literacy among surgical patients. Out of 40 studies involving 18,895 patients, it found that 31.7% had limited health literacy. The study highlighted the need for recognizing and addressing limited health literacy in surgical patients and called for standardization in health literacy measurement tools.

\[17\] This meta-analysis assessed the correlation between patient health literacy and adherence to medication and non-medication treatments. It found a positive association between health literacy and adherence, with the relationship being stronger in non-medication treatments and among patients with cardiovascular disease. Health literacy interventions were found to improve both health literacy and adherence, particularly in lower-income and racial-ethnic minority groups.

\[18\] This systematic review and meta-analysis evaluated the relationship between health literacy and medication adherence. The study, which included various databases and adhered to specific inclusion and exclusion criteria, found a small but statistically significant positive association between health literacy and medication adherence. The findings suggest that health literacy may play a mediating role in other adherence determinants, indicating a need for future research in this area.

\[19\] This study examines the impact of interventions designed to improve consumers’ online health literacy, which is their ability to search, evaluate, and use health information from the internet effectively. The research involved a comprehensive search of various databases for studies published between January 1990 and March 2008. It focused on randomized controlled trials \(RCTs), quasi-RCTs, and other study designs assessing interventions for online health literacy enhancement. Only two studies met the criteria for inclusion, one RCT and one controlled before and after \(CBA) study, with a total of 470 participants. The RCT showed positive effects in the intervention group for self-efficacy in health information seeking, evaluation skills, and discussion of online information with healthcare providers, while the CBA showed positive changes only in the readiness to use the internet for preventive health information among adults aged 50+. However, due to the limited number and varying quality of these studies, the evidence is not strong enough to make definitive conclusions about the effectiveness of these interventions. The authors recommend more well-designed RCTs involving diverse participant groups and settings to better understand the role of online health literacy in accessing health information on the internet.

Health literacy depends on diverse aspects of skills in processing information. To adequately understand medical reports, treatments and study results a set of abilities is needed.

AI in Healthcare

\[20\] discusses the significant impact of artificial intelligence \(AI), particularly large language models like ChatGPT, on the healthcare sector. ChatGPT is praised for its versatility in medical practices, including its role in pandemic management, surgical consultations, dental practices, medical education, and disease diagnosis. The study categorizes 82 papers into eight main areas related to healthcare applications. Despite its potential, integrating AI with human judgment remains a challenge. The systematic review, based on the PRISMA approach, highlights ChatGPT’s broad applications, limitations, and challenges in healthcare, suggesting it as a valuable resource for students, academics, and professionals in the field.

Die Integration von Artificial Intelligence \(AI) in das Gesundheitswesen birgt ein erhebliches Potenzial, von der Verbesserung der Gesundheitsbildung bis hin zur Optimierung klinischer Entscheidungsprozesse \[21\]; \[22\]. Der Einsatz von AI in der Gesundheitsfürsorge und Bildung ist jedoch komplex und wirft Fragen hinsichtlich Effizienz, Genauigkeit und ethischer Aspekte auf \[21\]; \[23\]. Die Integration von Künstlicher Intelligenz \(KI) in das Gesundheitswesen hat transformative Auswirkungen auf Bildung, Forschung und Praxis. Die potenziellen Anwendungen von KI-Systemen wie ChatGPT in der Gesundheitsbildung könnten vielversprechend sein, indem sie ….

Zudem erleichtern digitale Gesundheitslösungen, die auf KI basieren, die aktive Beteiligung der Patienten an ihrer Gesundheitsversorgung und können die Gesundheitskompetenz erhöhen \[22\]. Die Integration von Künstlicher Intelligenz \(KI) in den Gesundheitssektor verspricht enorme Fortschritte in der medizinischen Diagnostik und Behandlung. Die Anwendung von KI, insbesondere im Bereich der Dentalmedizin, hat das Potenzial, die Gesundheitsversorgung zu revolutionisieren, indem sie die Diagnostik effizienter und genauer macht und Behandlungspläne personalisiert. Die Rolle der KI im Gesundheitswesen wird immer wichtiger, da sie die Möglichkeit bietet, die Qualität der Patientenversorgung zu verbessern und gleichzeitig die Effizienz des Gesundheitssystems zu steigern \[24\]. Gesundheitskompetenz ist in einer sich schnell entwickelnden digitalen Welt von entscheidender Bedeutung, insbesondere im Kontext der Medizin und Gesundheitsversorgung. Die Integration von Künstlicher Intelligenz \(KI) in die Gesundheitsbildung und -kommunikation könnte die Art und Weise, wie Patienten und Fachleute Gesundheitsinformationen verstehen und nutzen, grundlegend verändern. KI-Technologien bieten das Potenzial, maßgeschneiderte Gesundheitsinformationen effizient zu vermitteln und so das Niveau der Gesundheitskompetenz zu erhöhen \[25\]. Eine Studie ergab, dass das Verständnis und die Anwendung von KI in der Medizin essenziell für die zukünftige Gesundheitsversorgung sind, wobei viele Medizinstudenten und Fakultätsmitglieder ein begrenztes Verständnis dieser Technologien zeigen \[26\].

=== Theoretical and/or empirical focus of the problem
<theoretical-andor-empirical-focus-of-the-problem>
Studien zeigen, dass AI, einschließlich großer Sprachmodelle wie ChatGPT, das Potenzial hat, Gesundheitsbildung zu verbessern, Forschung zu unterstützen und Gesundheitspraktiken zu vereinfachen \[21\].

\[27\] discusses the impact of ChatGPT on multiple-choice question \(MCQ) examinations in higher education. \[short version: The study reviewed the performance of ChatGPT on MCQ exams.\] Earlier versions \(GPT-3/3.5) underperformed compared to humans, while GPT-4 passed most exams at a level similar to human students. A review of 53 studies, covering 114 question sets with a total of 49,014 MCQs, was conducted. The findings showed that earlier versions of ChatGPT \(based on GPT-3/3.5) performed better than random guessing but generally failed most exams and underperformed compared to the average human student. However, GPT-4 passed most exams with results comparable to human subjects. The study concludes that to maintain the integrity of summative MCQ-based assessments, especially those testing foundational knowledge, such exams should be conducted in secure environments with restricted access to ChatGPT and similar AI tools.

Die Integration künstlicher Intelligenz \(KI) in medizinische Forschung und Praxis hat weitreichende Implikationen für die Effizienz und Qualität der Gesundheitsversorgung. ChatGPT, ein fortschrittliches KI-Modell, spielt eine zunehmende Rolle in der medizinischen Literatur und Forschung, insbesondere bei der Verarbeitung und Interpretation komplexer Daten \[28\]. Seine Fähigkeit, Ideen klar auszudrücken und allgemeine Kontexte verständlich zu formulieren, hebt sich hervor. Allerdings bestehen Bedenken hinsichtlich der Genauigkeit, des Bias und der ethischen Implikationen \[29\]; \[23\]). Trotz des Potenzials der KI gibt es Bedenken hinsichtlich ethischer, urheberrechtlicher, Transparenz- und rechtlicher Fragen sowie Risiken von Vorurteilen und ungenauen Inhalten \[21\].

Außerdem stellt sich die Frage, wie KI-Systeme die Health Literacy effektiv verbessern können, insbesondere unter Berücksichtigung unterschiedlicher sozialer und wirtschaftlicher Klassen \[30\]. In der aktuellen Literatur zeigt sich, dass KI in der Ausbildung von Gesundheitsfachkräften, insbesondere im Bereich der Kommunikationsfähigkeiten, unterstützend wirkt. Dies deutet darauf hin, dass KI eine wichtige Rolle in der Verbesserung der Health Literacy spielen könnte \[31\]. Gleichzeitig gibt es Hinweise darauf, dass KI in der Gesundheitspolitik genutzt wird, um evidenzbasierte Entscheidungen zu fördern \[32\]. Diese Entwicklungen unterstreichen die wachsende Bedeutung der KI in der Gesundheitskommunikation. Eine umfassende Kenntnis über KI ist für eine effektive Nutzung in der medizinischen Praxis unerlässlich. Evidenz: Eine kanadaweite Umfrage unter Gesundheitsstudierenden zeigte, dass 80% glauben, dass KI ihre Karrieren beeinflussen wird, aber es gibt erhebliche Lücken in ihrem Wissen über KI \[33\].

=== Focused problem statement
<focused-problem-statement>
Trotz der vielversprechenden Anwendungen von AI in der Gesundheitsbildung und -fürsorge besteht eine Diskrepanz zwischen den Erwartungen und der tatsächlichen Leistungsfähigkeit dieser Technologien. Insbesondere gibt es Bedenken hinsichtlich der Zuverlässigkeit der von AI bereitgestellten Informationen und deren Einfluss auf die Gesundheitskompetenz der Nutzer \[29\]; \[21\]. Trotz des Potenzials der KI in der Gesundheitsbildung bestehen Herausforderungen hinsichtlich Authentizität und Natürlichkeit der von KI-generierten Kommunikation, was die Wirksamkeit in realen klinischen Szenarien einschränken könnte \[31\]. Zudem gibt es Bedenken hinsichtlich der Voreingenommenheit in KI-Algorithmen, die die Fairness und Genauigkeit der durch KI vermittelten Informationen beeinträchtigen könnten \[34\].

Especially for those advising and informing people with less health literacy, it is important to have high competence in health literacy itselves.

When providing information to patients, medical doctors must be aware about patient health literacy and about their tools. Therefore, we conducted a study with ChatGPT for understanding its ability to interpret medical information provided. Viele medizinische Fachkräfte sind nicht ausreichend auf die Nutzung von KI-Technologien vorbereitet. Evidenz: Untersuchungen zeigen, dass medizinisches Personal oft nicht über grundlegende Kenntnisse in KI verfügt, was zu einer Kluft zwischen Erwartung und Wirklichkeit führt \[35\].

=== Statement of study intent
<statement-of-study-intent>
Eine mögliche Lösung könnte in der Entwicklung spezialisierter AI-Modelle liegen, die spezifisch für den Gesundheitssektor entwickelt und trainiert werden, um Genauigkeit, Verlässlichkeit und ethische Standards zu gewährleisten \[23\]; \[36\]. Dies würde einen Fortschritt in Richtung zuverlässiger und ethisch verantwortlicher AI-Systeme in der Gesundheitsbildung und -fürsorge bedeuten. Durch verbesserte Kenntnisse über KI können medizinische Fachkräfte diese Technologie effektiver nutzen. Evidenz: Studien betonen die Notwendigkeit, Health Literacy in Bezug auf KI in der medizinischen Ausbildung zu stärken, um ihre Potenziale voll auszuschöpfen \[37\]. Die Lösung dieser Herausforderungen könnte in der Entwicklung von KI-Systemen liegen, die nicht nur präzise und unvoreingenommene Informationen liefern, sondern auch die menschliche Dimension des Verständnisses und der Empathie berücksichtigen. Eine solche Integration von KI in die Health Literacy würde es ermöglichen, personalisierte und zuverlässige Gesundheitsinformationen bereitzustellen, wobei die Bedeutung der menschlichen Interaktion und des Vertrauens gewahrt bleibt \[38\].

We performed a study of medical students to investigate the following questions:

+ What is …
+ Why are …

== Methods
<methods>
=== Setting and subjects
<setting-and-subjects>
Our study was conducted at Medical Faculty of Münster, Germany. It takes six years to complete a course in medical school in Germany, with students enrolled directly from secondary schools. The course of study is divided into a pre-clinical section \(the first two years) and a clinical section \(the last four years). To improve students’ clinical experience, they are rotated in various hospital departments during their final year \("clinical/practical" year).

=== Study design
<study-design>
Graph literacy can be measured with the so-called 'graph literacy scale' introduced by Galesic et al.~in 2011 \[39\]. The participants were asked to complete the graph literacy scale voluntarily and anonymously.

=== Ethical approval
<ethical-approval>
Etiam quis tortor luctus, pellentesque ante a, finibus dolor. Phasellus in nibh et magna pulvinar malesuada. Ut nisl ex, sagittis at sollicitudin et, sollicitudin id nunc. In id porta urna. Proin porta dolor dolor, vel dapibus nisi lacinia in. Pellentesque ante mauris, ornare non euismod a, fermentum ut sapien. Proin sed vehicula enim. Aliquam tortor odio, vestibulum vitae odio in, tempor molestie justo. Praesent maximus lacus nec leo maximus blandit.

=== Data collection
<data-collection>
Data collection for this study was determined à priori as follows:

…

```{webr-r}
#| context: setup

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
)

# Read the data
df_penguins = read.csv("penguins.csv")
```

=== Outcome Measures
<outcome-measures>
…

=== Statistical methods
<statistical-methods>
…

```{webr-r}
#| context: interactive

# Download a dataset
download.file(
  'https://raw.githubusercontent.com/coatless/raw-data/main/penguins.csv',
  'penguins.csv'
) # Download the dataset

# Read the data
penguins = read.csv("penguins.csv")

# Scatterplot example: penguin bill length versus bill depth
ggplot2::ggplot(data = penguins, ggplot2::aes(x = bill_length_mm, y = bill_depth_mm)) + # Build a scatterplot
  ggplot2::geom_point(ggplot2::aes(color = species, 
                 shape = species),
             size = 2)  +
  ggplot2::scale_color_manual(values = c("darkorange","darkorchid","cyan4"))
```

== Results
<results>
=== Recruitment Process and Demographic Characteristics
<recruitment-process-and-demographic-characteristics>
The recruitment process is shown in Figure 1. We obtained XX complete data sets \(return rate YY.Z%) after contacting …

=== Primary and secondary Outcomes
<primary-and-secondary-outcomes>
#figure([
#box(width: 1682.0pt, image("Durchschnittswerte_Selbsteinschätzung_NKLM_14a.jpg"))
], caption: figure.caption(
position: bottom, 
[
Beispielgrafik: ein Bild sagt mehr als tausend Worte …
]), 
kind: "quarto-float-fig", 
supplement: "Figure", 
)


== Discussion
<discussion>
=== Summary
<summary>
After the evaluation of all datasets, the following findings emerged. The first is that …

=== Limitation: study population
<limitation-study-population>
Nullam dapibus cursus dolor sit amet consequat. Nulla facilisi. Curabitur vel nulla non magna lacinia tincidunt. Duis porttitor quam leo, et blandit velit efficitur ut. Etiam auctor tincidunt porttitor. Phasellus sed accumsan mi. Fusce ut erat dui. Suspendisse eu augue eget turpis condimentum finibus eu non lorem. Donec finibus eros eu ante condimentum, sed pharetra sapien sagittis. Phasellus non dolor ac ante mollis auctor nec et sapien. Pellentesque vulputate at nisi eu tincidunt. Vestibulum at dolor aliquam, hendrerit purus eu, eleifend massa. Morbi consectetur eros id tincidunt gravida. Fusce ut enim quis orci hendrerit lacinia sed vitae enim.

=== Limitation: study design
<limitation-study-design>
…

=== Integration with prior work
<integration-with-prior-work>
Only a few studies provide insights into the graphical and numerical skills among medical students.

In a cross-sectional, descriptive study, the researchers applied the Objective Numeracy, Subjective Numeracy, and Graph Literacy Scales to medical students in their final two years of medical school and to medical residents. The study included 169 participants, comprising 70% sixth-year seventh-year students, and 30% residents. The findings showed that the mean graph literacy was 10.35. A multiple linear regression analysis revealed that higher scores in the Graph Literacy Scale were associated with the male gender and younger age. The study concluded that numeracy and graph literacy scales’ mean scores were high among the medical students in this sample \[40\].

\[41\] examines the potential of using AI technologies like ChatGPT for academic writing, particularly in the field of health literacy. The authors conducted a descriptive study by asking ChatGPT five questions in both Portuguese and English with increasing complexity. The results showed that ChatGPT can generate structured and coherent academic texts in natural language. However, the study also highlights the need to consider the implications of AI in academic writing and knowledge dissemination, especially concerning issues like plagiarism and professional misconduct.

\[42\] compares the readability and quality of men’s health education materials from ChatGPT and the Urology Care Foundation™ \(UCF). ChatGPT responses were longer and more complex, and both sources failed to meet official readability standards, with ChatGPT performing worse. However, when ChatGPT was prompted to explain in simpler terms \("like I am in sixth grade"), its readability often surpassed UCF and even met recommended levels for two topics. While UCF and ChatGPT showed comparable accuracy, ChatGPT was more comprehensive but less understandable. The study concludes that AI-generated materials can be made more accessible with specific prompts, although neither AI nor provider-written content currently meets the recommended readability levels.

\[43\] evaluates the quality and readability of information provided by ChatGPT on four common hand surgeries, comparing it to materials from the American Society for Surgery of the Hand \(ASSH) HandCare website. The AI-generated responses were found to be of high quality but lacked citations and were written at a college reading level, which is higher than the ASSH materials \(eighth/ninth grade level). The AI responses emphasized the need for surgeon consultation and personalized care. The study concludes that while AI provides high-quality information, it has limitations regarding source transparency and readability, which are crucial for patient understanding in medical contexts.

\[44\] investigates the significance of sociodemographic variables in health literacy using machine learning methods. 1001 participants were assessed using the European Health Literacy Scale. Machine learning models, including XGBoost, random forest, and logistic regression, were employed. The XGBoost model showed the best performance. Significant differences in health literacy were observed across variables like gender, age, education, place of residence, economic situation, and health expense coverage. The most crucial variable was found to be newspaper reading, while the mother’s educational status was the least important. This model serves as a step towards enhancing physician-patient communication.

\[29\] evaluates the suitability and readability of responses generated by natural language processors, specifically ChatGPT, for urology-related patient inquiries. Eighteen questions based on Google Trends were assessed by three urologists for accuracy, comprehensiveness, and clarity. Responses were also evaluated for readability using Flesch Reading Ease and Flesch-Kincaid Grade Level formulas. Of the responses, 77.8% were deemed appropriate, but some lacked vital information. The study concludes that while natural language processors like ChatGPT have impressive capabilities, they require further refinement before being used for medical information dissemination.

\[45\] evaluates the accuracy, completeness, and readability of patient education material generated by ChatGPT, comparing it to content from the Society of Interventional Radiology Patient Center website. The analysis included word count, readability scales, factual correctness, and the PEMAT-P tool for patient education material assessment. The ChatGPT output was more extensive and difficult to read, with 11.5% incorrect responses for 104 questions. Both the website and ChatGPT content exceeded the recommended grade level for patient education. The study concludes that ChatGPT can produce incomplete or inaccurate content, and its current limitations should be recognized by healthcare providers.

\[46\] evaluated ChatGPT-4’s medical knowledge concerning vitreoretinal surgeries for retinal detachments, macular holes, and epiretinal membranes. Using questions asked on ChatGPT-4, responses were graded by retina specialists and assessed for readability. Results showed high appropriateness in responses \(84.6% to 92%) but indicated that the readability level was challenging, requiring college-level understanding. The study concludes that while ChatGPT-4 provides mostly appropriate responses, its current form is not a reliable source of factual information, particularly in specialized fields like medicine.

\[47\] explored the impact of a humanoid robot, Pepper, on individuals’ knowledge and attitudes towards influenza prevention and vaccination. Conducted in a hospital in Australia with 995 participants, the study used pre and post-tests and interviews. Significant improvements were observed in participants’ knowledge about flu prevention and attitudes towards vaccination after interacting with the robot. This suggests that humanoid robots can be an effective tool for health promotion and education in preventing influenza and other respiratory viruses.

\[48\] presents a conversational agent designed to simplify access to health information in Italian and improve health literacy. The agent enables users to query Patient Information Leaflets \(PILs) in natural language. The system was evaluated for performance and usability, showing adequate ability to handle dialogues and high user satisfaction. The study concludes that the conversational agent is a feasible and useful tool for enhancing access to and understanding of health information regarding medication.

\[49\] evaluates the impact of a robot-assisted learning system on the health literacy and learning perception of elderly individuals. It involved 60 participants over 50 years old, divided into two groups: one interacted with robots \(RobotLS group) and the other watched health education videos \(VideoLS group), both focusing on hypertension. Tools like the European Health Literacy Survey Questionnaire and others were used for assessment. The study found that the robot-assisted system significantly improved health knowledge, literacy, motivation, and flow perception compared to the video-assisted learning, suggesting its potential use in homes and care institutions for elderly health education.

\[50\] presents a big data-driven approach for predicting societal levels of health literacy. It utilizes latent Dirichlet allocation \(LDA) and convolutional neural network \(CNN) to analyze and predict health literacy from large volumes of public textual content. The approach involves feature extraction using CNN from health information, including lifestyle habits and health indicators, followed by model training and evaluation with common metrics like accuracy and precision. The model’s effectiveness is demonstrated through simulation experiments with real-world data, including a case study comparing health literacy between China and developed countries.

\[51\] The ECLIPPSE Project aims to measure patients’ health literacy and physicians’ linguistic complexity using natural language processing \(NLP) and machine learning \(ML) on over 400,000 secure messages exchanged via an electronic patient portal. The study faced challenges in data mining, analyses using NLP, and interdisciplinary collaboration. Solutions involved cleaning and culling messages, selecting appropriate computational indices, and facilitating interdisciplinary understanding and priorities. The study highlights the potential of using big linguistic data in health literacy research and the importance of addressing diverse challenges in similar studies.

\[52\] focused on using natural language processing and machine learning to identify health literacy levels from secure patient messages in the ECLIPPSE study. The aim was to develop automated "literacy profiles" as indicators of health literacy, avoiding the need for intrusive self-reporting by patients. The study used 283,216 messages from 6,941 patients, part of Kaiser Permanente Northern California’s DISTANCE Study. These profiles were compared with self-reported health literacy levels. Key findings included correlations between limited health literacy profiles and factors like older age, minority status, poor medication adherence, poor glycemic control, and higher rates of hypoglycemia and healthcare utilization. The study demonstrated the potential of using natural language processing as a non-intrusive, economical method to assess patient health literacy and identify those at risk of poor health outcomes.

\[53\] examined the impact of artificial intelligence \(AI) on patient health behavior, focusing on the concepts of health literacy and patient empowerment. It highlighted the challenges patients face in using the internet effectively for health-related information and the limitations in leveraging AI for health benefits. The study emphasized the need for substantial guidance to improve patients’ health literacy in the digital age. It stressed the importance of directing patients towards reliable online resources and educating them on interpreting the information found. The key conclusion was that healthcare professionals should guide and educate patients in their internet searches and information interpretation to fully realize the benefits of AI in healthcare.

\[54\] explored using the Chat Generative Pre-Trained Transformer \(ChatGPT) for creating patient education materials about implant-based breast reconstruction and compared its performance with expert-generated content. The comparison focused on readability \(measured by Flesch-Kincaid score) and accuracy \(evaluated by independent reviewers). Results showed that expert-generated content was more readable \(7.5 grade level) compared to ChatGPT \(10.5 grade level), even though ChatGPT was instructed to target a 7th-grade level. ChatGPT’s accuracy was 50%, with most errors being informational, often providing unrelated information. However, ChatGPT significantly reduced the time required to generate content \(30 minutes compared to a month for experts). The study concluded that while ChatGPT is a useful starting tool for creating patient education materials, its readability and accuracy need improvement.

=== Implications for practice
<implications-for-practice>
Etiam congue quam eget velit convallis, eu sagittis orci vestibulum. Vestibulum at massa turpis. Curabitur ornare ex sed purus vulputate, vitae porta augue rhoncus. Phasellus auctor suscipit purus, vel ultricies nunc. Nunc eleifend nulla ac purus volutpat, id fringilla felis aliquet. Duis vitae porttitor nibh, in rhoncus risus. Vestibulum a est vitae est tristique vehicula. Proin mollis justo id est tempus hendrerit. Praesent suscipit placerat congue. Aliquam eu elit gravida, consequat augue non, ultricies sapien. Nunc ultricies viverra ante, sit amet vehicula ante volutpat id. Etiam tempus purus vitae tellus mollis viverra. Donec at ornare mauris. Aliquam sodales hendrerit ornare. Suspendisse accumsan lacinia sapien, sit amet imperdiet dui molestie ut.

=== Implications for research
<implications-for-research>
…

Relying on artificial intelligence \(AI) for health literacy presents potential risks that need to be carefully considered. Several studies have explored this topic, highlighting important concerns. For example, a study on the use of an AI robot intervention prototype for weight-loss management found that while AI can effectively encourage individuals to make decisions regarding weight management, the health information presented should be adjusted based on the patient’s level of health literacy \[36\].

=== Conclusions
<conclusions>
…

#block[
#heading(
level: 
2
, 
numbering: 
none
, 
[
References
]
)
]
#block[
#block[
1. Baumeister A, Aldin A, Chakraverty D, Hübner C, Adams A, Monsef I, et al. #link("https://doi.org/10.1002/14651858.cd013303.pub2")[Interventions for improving health literacy in migrants];. Cochrane Database of Systematic Reviews. 2023;2023.

] <ref-Baumeister2023>
#block[
2. Verweel L, Newman A, Michaelchuk W, Packham T, Goldstein R, Brooks D. #link("https://doi.org/10.1016/j.ijmedinf.2023.105114")[The effect of digital interventions on related health literacy and skills for individuals living with chronic diseases: A systematic review and meta-analysis];. International Journal of Medical Informatics. 2023;177:105114.

] <ref-Verweel2023>
#block[
3. Yan L, Pu C, Rastogi S, Choudhury R, Shekar MK, Talukdar G. #link("https://doi.org/10.17219/acem/162538")[Evaluating the influence of health literacy and health-promoting COVID-19 protective behaviors on the spread of infection during the COVID-19 pandemic: A meta-analysis];. Advances in Clinical and Experimental Medicine. 2023;32:1357–68.

] <ref-Yan2023>
#block[
4. McAnally K, Hagger MS. #link("https://doi.org/10.1037/hea0001266")[Health literacy, social cognition constructs, and health behaviors and outcomes: A meta-analysis.] Health Psychology. 2023;42:213–34.

] <ref-McAnally2023>
#block[
5. Ran X, Chen Y, Jiang K, Shi Y. #link("https://doi.org/10.3390/ijerph192013078")[The Effect of Health Literacy Intervention on Patients with Diabetes: A Systematic Review and Meta-Analysis];. International Journal of Environmental Research and Public Health. 2022;19:13078.

] <ref-Ran2022>
#block[
6. Singh H, Kolschen J, Samkange-Zeeb F, Brand T, Zeeb H, Schüz B. #link("https://doi.org/10.1186/s12889-022-13851-0")[Modifiable predictors of health literacy in working-age adults - a rapid review and meta-analysis];. BMC Public Health. 2022;22.

] <ref-Singh2022>
#block[
7. Chakraverty D, Baumeister A, Aldin A, Seven ÜS, Monsef I, Skoetz N, et al. #link("https://doi.org/10.1136/bmjopen-2021-056090")[Gender differences of health literacy in persons with a migration background: a systematic review and meta-analysis];. BMJ Open. 2022;12:e056090.

] <ref-Chakraverty2022>
#block[
8. Kanejima Y, Shimogai T, Kitamura M, Ishihara K, Izawa KP. #link("https://doi.org/10.1016/j.pec.2021.11.021")[Impact of health literacy in patients with cardiovascular diseases: A systematic review and meta-analysis];. Patient Education and Counseling. 2022;105:1793–800.

] <ref-Kanejima2022>
#block[
9. Muscat DM, Smith J, Mac O, Cadet T, Giguere A, Housten AJ, et al. #link("https://doi.org/10.1177/0272989x211011101")[Addressing Health Literacy in Patient Decision Aids: An Update from the International Patient Decision Aid Standards];. Medical Decision Making. 2021;41:848–69.

] <ref-Muscat2021>
#block[
10. Lim ML, Schooten KS van, Radford KA, Delbaere K. #link("https://doi.org/10.1093/heapro/daaa072")[Association between health literacy and physical activity in older people: a systematic review and meta-analysis];. Health Promotion International. 2021;36:1482–97.

] <ref-Lim2021>
#block[
11. Baccolini V, Rosso A, Di Paolo C, Isonne C, Salerno C, Migliara G, et al. #link("https://doi.org/10.1007/s11606-020-06407-8")[What is the Prevalence of Low Health Literacy in European Union Member States? A Systematic Review and Meta-analysis];. Journal of General Internal Medicine. 2021;36:753–61.

] <ref-Baccolini2021>
#block[
12. Fabbri M, Murad MH, Wennberg AM, Turcano P, Erwin PJ, Alahdab F, et al. #link("https://doi.org/10.1016/j.jchf.2019.11.007")[Health Literacy and Outcomes Among Patients With Heart~Failure];. JACC: Heart Failure. 2020;8:451–60.

] <ref-Fabbri2020>
#block[
13. Abdullah A, Liew SM, Salim H, Ng CJ, Chinna K. #link("https://doi.org/10.1371/journal.pone.0216402")[Prevalence of limited health literacy among patients with type 2 diabetes mellitus: A systematic review];. PLOS ONE. 2019;14:e0216402.

] <ref-Abdullah2019>
#block[
14. Liu XB, Ayatollahi Y, Yamashita T, Jaradat M, Shen JJ, Kim SJ, et al. #link("https://doi.org/10.3928/19404921-20181018-01")[Health Literacy and Mortality in Patients With Heart Failure: A Systematic Review and Meta-Analysis];. Research in Gerontological Nursing. 2019;12:99–108.

] <ref-Liu2019>
#block[
15. Zheng M, Jin H, Shi N, Duan C, Wang D, Yu X, et al. #link("https://doi.org/10.1186/s12955-018-1031-7")[The relationship between health literacy and quality of life: a systematic review and meta-analysis];. Health and Quality of Life Outcomes. 2018;16.

] <ref-Zheng2018>
#block[
16. Roy M, Corkum JP, Urbach DR, Novak CB, Schroeder HP von, McCabe SJ, et al. #link("https://doi.org/10.1007/s00268-018-4754-z")[Health Literacy Among Surgical Patients: A Systematic Review and Meta-analysis];. World Journal of Surgery. 2018;43:96–106.

] <ref-Roy2019>
#block[
17. Miller TA. #link("https://doi.org/10.1016/j.pec.2016.01.020")[Health literacy and adherence to medical treatment in chronic and acute illness: A meta-analysis];. Patient Education and Counseling. 2016;99:1079–86.

] <ref-Miller2016>
#block[
18. Zhang NJ, Terry A, McHorney CA. #link("https://doi.org/10.1177/1060028014526562")[Impact of Health Literacy on Medication Adherence];. Annals of Pharmacotherapy. 2014;48:741–51.

] <ref-Zhang2014>
#block[
19. Car J, Lang B, Colledge A, Ung C, Majeed A. Interventions for enhancing consumers’ online health literacy. Cochrane Database of Systematic Reviews. 2011. #link("https://doi.org/10.1002/14651858.cd007092.pub2");.

] <ref-Car2011>
#block[
20. Younis HA, Eisa TAE, Nasser M, Sahib TM, Noor AA, Alyasiri OM, et al. #link("https://doi.org/10.3390/diagnostics14010109")[A Systematic Review and Meta-Analysis of Artificial Intelligence Tools in Medicine and Healthcare: Applications, Considerations, Limitations, Motivation and Challenges];. Diagnostics. 2024;14:109.

] <ref-Younis2024>
#block[
21. Sallam M. #link("https://doi.org/10.3390/healthcare11060887")[ChatGPT Utility in Healthcare Education, Research, and Practice: Systematic Review on the Promising Perspectives and Valid Concerns];. Healthcare. 2023;11:887.

] <ref-Sallam2023>
#block[
22. Conard S. #link("https://doi.org/10.1016/j.ijcard.2019.05.070")[Best practices in digital health literacy];. International Journal of Cardiology. 2019;292:277–9.

] <ref-Conard2019>
#block[
23. Malerbi FK, Nakayama LF, Gayle Dychiao R, Zago Ribeiro L, Villanueva C, Celi LA, et al. #link("https://doi.org/10.2196/43333")[Digital Education for the Deployment of Artificial Intelligence in Health Care];. Journal of Medical Internet Research. 2023;25:e43333.

] <ref-Malerbi2023>
#block[
24. Schwendicke F, Samek W, Krois J. #link("https://doi.org/10.1177/0022034520915714")[Artificial Intelligence in Dentistry: Chances and Challenges];. Journal of Dental Research. 2020;99:769–74.

] <ref-Schwendicke2020>
#block[
25. Martinez-Millana A, Saez-Saez A, Tornero-Costa R, Azzopardi-Muscat N, Traver V, Novillo-Ortiz D. #link("https://doi.org/10.1016/j.ijmedinf.2022.104855")[Artificial intelligence and its impact on the domains of universal health coverage, health emergencies and health promotion: An overview of systematic reviews];. International Journal of Medical Informatics. 2022;166:104855.

] <ref-Martinez-Millana2022>
#block[
26. Wood EA, Ange BL, Miller DD. #link("https://doi.org/10.1177/23821205211024078")[Are We Ready to Integrate Artificial Intelligence Literacy into Medical School Curriculum: Students and Faculty Survey];. Journal of Medical Education and Curricular Development. 2021;8:238212052110240.

] <ref-Wood2021>
#block[
27. Newton P, Xiromeriti M. #link("https://doi.org/10.1080/02602938.2023.2299059")[ChatGPT performance on multiple choice question examinations in higher education. A pragmatic scoping review];. Assessment & Evaluation in Higher Education. 2024;1–18.

] <ref-newton2024>
#block[
28. Gödde D, Nöhl S, Wolf C, Rupert Y, Rimkus L, Ehlers J, et al. #link("https://doi.org/10.2196/49368")[A SWOT \(Strengths, Weaknesses, Opportunities, and Threats) Analysis of ChatGPT in the Medical Literature: Concise Review];. Journal of Medical Internet Research. 2023;25:e49368.

] <ref-Gödde2023>
#block[
29. Davis R, Eppler M, Ayo-Ajibola O, Loh-Doyle JC, Nabhani J, Samplaski M, et al. #link("https://doi.org/10.1097/ju.0000000000003615")[Evaluating the Effectiveness of Artificial Intelligencepowered Large Language Models Application in Disseminating Appropriate and Readable Health Information in Urology];. Journal of Urology. 2023;210:688–94.

] <ref-Davis2023>
#block[
30. Hammond WE, West VL. #link("https://doi.org/10.3233/shti231007")[Making digital health equitable];. IOS Press; 2024.

] <ref-Hammond2024>
#block[
31. Stamer T, Steinhäuser J, Flägel K. #link("https://doi.org/10.2196/43311")[Artificial Intelligence Supporting the Training of Communication Skills in the Education of Health Care Professions: Scoping Review];. Journal of Medical Internet Research. 2023;25:e43311.

] <ref-Stamer2023>
#block[
32. Ramezani M, Takian A, Bakhtiari A, Rabiee HR, Ghazanfari S, Mostafavi H. #link("https://doi.org/10.1186/s12913-023-10462-2")[The application of artificial intelligence in health policy: a scoping review];. BMC Health Services Research. 2023;23.

] <ref-Ramezani2023>
#block[
33. Teng MY, Singla R, Yau O, Lamoureux D, Gupta A, Hu Z, et al. What Do Healthcare Student Want to Know About Artificial Intelligence? A Canada-Wide Survey. SSRN Electronic Journal. 2021. #link("https://doi.org/10.2139/ssrn.3900405");.

] <ref-teng2021>
#block[
34. Vorisek CN, Stellmach C, Mayer PJ, Klopfenstein SAI, Bures DM, Diehl A, et al. #link("https://doi.org/10.2196/41089")[Artificial Intelligence Bias in Health Care: Web-Based Survey];. Journal of Medical Internet Research. 2023;25:e41089.

] <ref-Vorisek2023>
#block[
35. Rashidi HH, Tran NK, Betts EV, Howell LP, Green R. #link("https://doi.org/10.1177/2374289519873088")[Artificial Intelligence and Machine Learning in Pathology: The Present Landscape of Supervised Methods];. Academic Pathology. 2019;6:2374289519873088.

] <ref-Rashidi2019>
#block[
36. Chu Y-T, Huang R-Y, Chen TT-W, Lin W-H, Tang JT, Lin C-W, et al. #link("https://doi.org/10.1177/20552076221136372")[Effect of health literacy and shared decision-making on choice of weight-loss plan among overweight or obese participants receiving a prototype artificial intelligence robot intervention facilitating weight-loss management decisions];. DIGITAL HEALTH. 2022;8:205520762211363.

] <ref-Chu2022a>
#block[
37. Babic B, Gerke S, Evgeniou T, Cohen IG. #link("https://doi.org/10.1038/s42256-021-00331-0")[Direct-to-consumer medical machine learning and artificial intelligence applications];. Nature Machine Intelligence. 2021;3:283–7.

] <ref-babic2021>
#block[
38. Gould DJ, Dowsey MM, Glanville-Hearst M, Spelman T, Bailey JA, Choong PFM, et al. #link("https://doi.org/10.2196/43632")[Patients’ Views on AI for Risk Prediction in Shared Decision-Making for Knee Replacement Surgery: Qualitative Interview Study];. Journal of Medical Internet Research. 2023;25:e43632.

] <ref-Gould2023>
#block[
39. Galesic M, Garcia-Retamero R. Graph literacy: A cross-cultural comparison. Medical Decision Making. 2011;31:444457.

] <ref-galesic2011>
#block[
40. Mas G, Tello T, Ortiz P, Petrova D, García-Retamero R. #link("https://doi.org/10.24875/gmm.17003024")[Habilidad gráfica y numérica en estudiantes de medicina de pre y posgrado de una universidad privada];. Gaceta Médica de México. 2018;154.

] <ref-Mas2018>
#block[
41. Peres F. #link("https://doi.org/10.1590/1413-81232024291.02412023")[A literacia em saúde no ChatGPT: Explorando o potencial de uso de inteligência artificial para a elaboração de textos acadêmicos];. Ciência & Saúde Coletiva. 2024;29.

] <ref-Peres2024>
#block[
42. Shah YB, Ghosh A, Hochberg AR, Rapoport E, Lallas CD, Shah MS, et al. #link("https://doi.org/10.1097/upj.0000000000000490")[Comparison of ChatGPT and Traditional Patient Education Materials for Men’s Health];. Urology Practice. 2024;11:87–94.

] <ref-shah2024>
#block[
43. Crook BS, Park CN, Hurley ET, Richard MJ, Pidgeon TS. #link("https://doi.org/10.1016/j.jhsa.2023.08.003")[Evaluation of Online Artificial Intelligence-Generated Information on Common Hand Procedures];. The Journal of Hand Surgery. 2023;48:1122–7.

] <ref-Crook2023>
#block[
44. İnceoğlu F, Deniz S, Yagin FH. #link("https://doi.org/10.1016/j.ijmedinf.2023.105167")[Prediction of effective sociodemographic variables in modeling health literacy: A machine learning approach];. International Journal of Medical Informatics. 2023;178:105167.

] <ref-Inceoglu2023>
#block[
45. McCarthy CJ, Berkowitz S, Ramalingam V, Ahmed M. #link("https://doi.org/10.1016/j.jvir.2023.05.037")[Evaluation of an Artificial Intelligence Chatbot for Delivery of IR Patient Education Material: A Comparison with Societal Website Content];. Journal of Vascular and Interventional Radiology. 2023;34:1760–1768.e32.

] <ref-McCarthy2023>
#block[
46. Momenaei B, Wakabayashi T, Shahlaee A, Durrani AF, Pandit SA, Wang K, et al. #link("https://doi.org/10.1016/j.oret.2023.05.022")[Appropriateness and Readability of ChatGPT-4-Generated Responses for Surgical Treatment of Retinal Diseases];. Ophthalmology Retina. 2023;7:862–8.

] <ref-Momenaei2023>
#block[
47. McIntosh C, Elvin A, Smyth W, Birks M, Nagle C. #link("https://doi.org/10.1177/00469580221078515")[Health Promotion, Health Literacy and Vaccine Hesitancy: The Role of Humanoid Robots];. INQUIRY: The Journal of Health Care Organization, Provision, and Financing. 2022;59:004695802210785.

] <ref-McIntosh2022>
#block[
48. Minutolo A, Damiano E, De Pietro G, Fujita H, Esposito M. #link("https://doi.org/10.1016/j.compbiomed.2021.105004")[A conversational agent for querying Italian Patient Information Leaflets and improving health literacy];. Computers in Biology and Medicine. 2022;141:105004.

] <ref-Minutolo2022>
#block[
49. Wei C-W, Kao H-Y, Wu W-H, Chen C-Y, Fu H-P. #link("https://doi.org/10.3390/ijerph182111053")[The Influence of Robot-Assisted Learning System on Health Literacy and Learning Perception];. International Journal of Environmental Research and Public Health. 2021;18:11053.

] <ref-Wei2021>
#block[
50. Zhao X, Ding S. #link("https://doi.org/10.3934/mbe.2023804")[A multi-dimension information fusion-based intelligent prediction approach for health literacy];. Mathematical Biosciences and Engineering. 2023;20:18104–22.

] <ref-Zhao2023>
#block[
51. Brown W, Balyan R, Karter AJ, Crossley S, Semere W, Duran ND, et al. #link("https://doi.org/10.1016/j.jbi.2020.103658")[Challenges and solutions to employing natural language processing and machine learning to measure patients’ health literacy and physician writing complexity: The ECLIPPSE study];. Journal of Biomedical Informatics. 2021;113:103658.

] <ref-Brown2021>
#block[
52. Balyan R, Crossley SA, Brown W, Karter AJ, McNamara DS, Liu JY, et al. #link("https://doi.org/10.1371/journal.pone.0212488")[Using natural language processing and machine learning to classify health literacy from secure messages: The ECLIPPSE study];. PLOS ONE. 2019;14:e0212488.

] <ref-Balyan2019>
#block[
53. Schulz PJ, Nakamoto K. #link("https://doi.org/10.1016/j.pec.2013.05.002")[Patient behavior and the benefits of artificial intelligence: The perils of “dangerous” literacy and illusory patient empowerment];. Patient Education and Counseling. 2013;92:223–8.

] <ref-Schulz2013>
#block[
54. Hung Y-C, Chaker SC, Sigel M, Saad M, Slater ED. #link("https://doi.org/10.1097/sap.0000000000003634")[Comparison of Patient Education Materials Generated by Chat Generative Pre-Trained Transformer Versus Experts];. Annals of Plastic Surgery. 2023;91:409–12.

] <ref-Hung2023>
] <refs>
== Declarations
<declarations>
=== Ethics approval and consent to participate
<ethics-approval-and-consent-to-participate>
Etiam congue quam eget velit convallis, eu sagittis orci vestibulum. Vestibulum at massa turpis. Curabitur ornare ex sed purus vulputate, vitae porta augue rhoncus. Phasellus auctor suscipit purus, vel ultricies nunc. Nunc eleifend nulla ac purus volutpat, id fringilla felis aliquet. Duis vitae porttitor nibh, in rhoncus risus. Vestibulum a est vitae est tristique vehicula. Proin mollis justo id est tempus hendrerit. Praesent suscipit placerat congue. Aliquam eu elit gravida, consequat augue non, ultricies sapien. Nunc ultricies viverra ante, sit amet vehicula ante volutpat id. Etiam tempus purus vitae tellus mollis viverra. Donec at ornare mauris. Aliquam sodales hendrerit ornare. Suspendisse accumsan lacinia sapien, sit amet imperdiet dui molestie ut.

=== Consent for publication
<consent-for-publication>
Not applicable

=== Availability of data and materials
<availability-of-data-and-materials>
The original data that support the findings of this study are available from Open Science Framework \(osf.io, see manuscript-URL).

=== Competing interests
<competing-interests>
The authors declare that they have no competing interests.

=== Funding
<funding>
The author\(s) received no specific funding for this work.

=== Authors’ contributions
<authors-contributions>
Aenean placerat luctus tortor vitae molestie. Nulla at aliquet nulla. Sed efficitur tellus orci, sed fringilla lectus laoreet eget. Vivamus maximus quam sit amet arcu dignissim, sed accumsan massa ullamcorper. Sed iaculis tincidunt feugiat. Nulla in est at nunc ultricies dictum ut vitae nunc. Aenean convallis vel diam at malesuada. Suspendisse arcu libero, vehicula tempus ultrices a, placerat sit amet tortor. Sed dictum id nulla commodo mattis. Aliquam mollis, nunc eu tristique faucibus, purus lacus tincidunt nulla, ac pretium lorem nunc ut enim. Curabitur eget mattis nisl, vitae sodales augue. Nam felis massa, bibendum sit amet nulla vel, vulputate rutrum lacus. Aenean convallis odio pharetra nulla mattis consequat.

=== CRediT authorship contribution statement
<credit-authorship-contribution-statement>
#strong[Janina Soler Wenglein:] Data curation, Formal analysis, Investigation, Methodology, Visualization, Writing - original draft. \
#strong[Hendrik Friederichs:] Conceptualization, Formal analysis, Investigation, Methodology, Supervision, Writing - review & editing, Writing - original draft.

=== Acknowledgments
<acknowledgments>
The authors are grateful for the insightful comments offered by the anonymous peer reviewers at Ziel-Journal. The generosity and expertise of one and all have improved this study in innumerable ways and saved us from many errors; those that inevitably remain are entirely our own responsibility.
